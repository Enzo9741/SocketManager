from django import forms


class HostForm(forms.Form):
    hostname = forms.CharField()
    ip = forms.CharField()

    hostname.widget.attrs.update({'class': 'form-control'})
    ip.widget.attrs.update({'class': 'form-control'})
