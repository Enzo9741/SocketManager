from django import forms


class SocketForm(forms.Form):

    port = forms.IntegerField()
    socket_name = forms.CharField()
    socket_description = forms.CharField()

    port.widget.attrs.update({'class': 'form-control'})
    socket_name.widget.attrs.update({'class': 'form-control'})
    socket_description.widget.attrs.update({'class': 'form-control'})
