# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Host(models.Model):
    id = models.IntegerField(primary_key=True)
    hostname = models.CharField(max_length=128)
    ip = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'host'

    def __str__(self):
        return '{} | {} '.format(self.hostname, self.ip)

class Socket(models.Model):
    id = models.IntegerField(primary_key=True)
    id_host = models.ForeignKey(Host, models.DO_NOTHING, db_column='id_host')
    port = models.IntegerField(blank=True, null=True)
    socket_name = models.CharField(max_length=128)
    socket_description = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'socket'

    def __str__(self):
        return '{} | {}:{} '.format(self.socket_name, Host.objects.get(id=self.id_host.id).ip, self.port)
