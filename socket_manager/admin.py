from django.contrib import admin

from socket_manager.models import Host, Socket

admin.site.register(Host)
admin.site.register(Socket)
