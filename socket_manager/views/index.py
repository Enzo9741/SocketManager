from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from socket_manager.models import Host, Socket

@login_required(login_url='/admin/login?next=/')
def index(request):
    hosts = Host.objects.all()
    sockets = Socket.objects.all()

    return render(request, 'index.html', {'hosts': hosts, 'sockets': sockets})
