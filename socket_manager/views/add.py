from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from socket_manager.forms.hostForm import HostForm
from socket_manager.forms.socketForm import SocketForm
from socket_manager.models import Host, Socket


@login_required(login_url='/admin/login?next=/')
def add(request):
    if request.method == "POST":
        post = request.POST

        if post['form_type'] == "host-form":
            Host(hostname=post['hostname'], ip=post['ip']).save()
        elif post['form_type'] == "socket-form":
            Socket(id_host=Host.objects.get(id=post['selector']), socket_name=post['socket_name'], socket_description=post['socket_description'], port=post['port']).save()

    host_form = HostForm()
    socket_form = SocketForm()
    hosts = Host.objects.all()
    return render(request, 'add.html', {'host': host_form, 'socket': socket_form, 'hosts': hosts })
