from django.apps import AppConfig


class SocketManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'socket_manager'
