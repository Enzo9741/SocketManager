CREATE DATABASE IF NOT EXISTS socket_manager;
USE socker_manager;

CREATE TABLE IF NOT EXISTS host (
    id INT(11) NOT NULL AUTO_INCREMENT,
    hostname VARCHAR(128) NOT NULL,
    ip VARCHAR(32) NOT NULL,

    CONSTRAINT pk_host PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS socket (
    id INT(11) NOT NULL AUTO_INCREMENT,
    id_host INT(11) NOT NULL,
    port INT(11),
    socket_name VARCHAR(128) NOT NULL,
    socket_description VARCHAR(128) NULL,

    CONSTRAINT pk_socket PRIMARY KEY (id),
    CONSTRAINT fk_host_socket FOREIGN KEY (id_host) REFERENCES host(id) ON DELETE CASCADE
);
